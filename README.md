# AWS S3 Bucket Sync

This repo is an example of syncing data with AWS S3-bucket using GitLab-CI.

The terraform module used in this example:

https://github.com/thePaulRichard/terraform-aws-s3-cloudfront.git

## Getting started

1. Clone this repo.
1. Change the main.tf file as your needs. See [terraform-aws-s3-cloudfront](https://github.com/thePaulRichard/terraform-aws-s3-cloudfront.git) for more.
1. Execute the following commands:
    - `sh gpg.sh` (this will create the 'key' file with the gpg key inside that we'll use to create the 'AccessKey' of the user)
    - `terraform init`
    - `terraform apply`
1. In the case you use **gpg** when creating the IAM user, execute the following to decrypt the IAM secret:
    - `terraform output iam_secret | base64 --decode --ignore-garbage | gpg --decrypt`
1. Create the following variables in the GitLab and put the values of the terraform output (with the iam_secret decrypted):
    - **AWS_DEFAULT_REGION** (the region where you create the resources)
    - **AWS_ACCESS_KEY_ID** (iam_access_key)
    - **AWS_SECRET_ACCESS_KEY** (iam_secret)
    - **CLOUDFRONT** (cloudfront_id)
    - **S3_BUCKET** (s3_bucket_id)
1. `git push`
1. After the pipeline succeeds, you can remove the gpg key in your machine with the following command:
    - `gpg --delete-secret-and-public-keys terraform`

Any changes inside the **s3bucket** folder will trigger the pipeline. 

## Re-create the credentials

The terraform module will ignore any changes on the **key** file, so if you want to re-create the 'AccessKey' or the 'IAM User', we need to terraform to re-create the resource.

After creating a new gpg key, use the following command to recreate the Access Key for the IAM User:

`terraform taint module.s3_cloudfront.aws_iam_access_key.ci_cd[0]`

If you want to recreate the IAM User, use the following:

`terraform taint module.s3_cloudfront.aws_iam_user.ci_cd[0]`
